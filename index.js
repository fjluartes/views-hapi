'use strict';

const Hapi = require('hapi');
const Vision = require('vision');
const Handlebars = require('handlebars');
const Inert = require('inert');

const server = new Hapi.server({
  port: 3000,
  host: 'localhost'
});

const init = async () => {
  await server.register([{
    plugin: require('hapi-pino'),
    options: {
      prettyPrint: true,
      logEvents: ['response']
    }
  }, Vision, Inert]);

  server.views({
    engines: {
      html: Handlebars
    },
    path: 'website/contents',
    layoutPath: 'website',
    layout: 'index',
    partialsPath: 'website/partials',
    helpersPath: 'website/helpers'
  });

  server.route(require('./routes'));

  await server.start();
  console.log(`Server listening at ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();
