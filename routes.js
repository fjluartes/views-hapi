'use strict';

const websitePath = './website/';
const menuJSON = require('./data/menu.json');
const socialJSON = require('./data/social_links.json');
const postOne = require('./data/postOne.json');

module.exports = [
  {
    method: 'GET',
    path: '/css/{file}',
    handler: (request, h) => {
      return h.file(websitePath + 'css/' + request.params.file);
    }
  },
  {
    method: 'GET',
    path: '/script/{file}',
    handler: (request, h) => {
      return h.file(websitePath + 'script/' + request.params.file);
    }
  },
  {
    method: 'GET',
    path: '/',
    handler: (request, h) => {
      return h.view('front-page', { menu: menuJSON, social: socialJSON });
    }
  },
  {
    method: 'GET',
    path: '/about',
    handler: (request, h) => {
      return h.view('about', { menu: menuJSON, social: socialJSON });
    }
  },
  {
    method: 'GET',
    path: '/post/postOne',
    handler: (request, h) => {
      return h.view('post', { menu: menuJSON, social: socialJSON, postContent: postOne });
    }
  },
  {
    method: 'GET',
    path: '/{path*}',
    handler: (request, h) => {
      return h.view('404', { menu: menuJSON, social: socialJSON }).code(404);
    }
  }
];
